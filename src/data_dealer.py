import time
from datetime import datetime
from random import randint
from asn1_b import *
from pyasn1.codec.ber import encoder, decoder
from dealer_protocol import DealerProtocol
from twisted.internet.task import LoopingCall
from twisted.internet import reactor

import zmq


class Sensor(object):

    def __init__(self, _id, _type):
        self._id = _id
        self._type = _type
        self._value = []

    def configure(self, time):
            print("Sensor #%s will generate value every %s seconds" % (self._id,
                                                                       time))
            self.lc = LoopingCall(self.generate_value)
            self.lc.start(time)

    def generate_value(self):
        try:
            value = randint(1, 100)
            timestamp = datetime.now()
            print("[%s] Sensor #%s generated %s" % (timestamp, self._id,
                                                   value))
            timestamp = int(time.mktime(timestamp.timetuple()))
            self._value.append((value, timestamp))
        except Exception as e:
            print("Generating value error %s" % e)

    def get_id(self):
        return self._id

    def get_type(self):
        return self._type

    def get_values(self):
        return self._value

    def clear_values(self):
        self._value = []


class BoardProtocol(object):

    SENSOR_TYPE_TEMP = 1
    SENSOR_TYPE_IR = 2
    SENSOR_TYPE_CAMERA = 3

    LOG_TYPE = {
        1: 'Temperature',
        2: 'Infrared',
        3: 'Camera'
    }

    def __init__(self, host, port, *args):
        self._id = randint(1, 10000)
        self.sensors = []
        self._values = {}
        self._sensor_id = 1
        self._send_time = None
        self._sample = None
        self._alarm = None
        self._dealer = DealerProtocol(host, port, 'client')
        if args:
            self._register_sensor(args)

    def _register_sensor(self, _types):
        for tp in _types:
            if int(tp) in range(1, 4):
                sensor_id = self._new_id()
                print("Registering sensor #%s of type '%s'"
                      % (sensor_id, self.LOG_TYPE[int(tp)]))
                new_sensor = Sensor(sensor_id, int(tp))
                self.sensors.append(new_sensor)

    def _new_id(self):
        id = self._sensor_id
        self._sensor_id += 1
        return id

    def _register_on_server(self):
        print("Registering on server")
        sensor_desc = []
        for item in self.sensors:
            sensor_desc.append((item.get_id(), item.get_type()))
        data = (self._id, sensor_desc)
        self._dealer.send_identification(data)
        response = self._dealer.recv()
        self._configure_resources(response)

    def _configure_resources(self, configuration):
        print("INFO: Received configuration from server")
        print("Send time: %s" % configuration['sendTime'])
        print("Sample time: %s" % configuration['sample'])
        print("Alarm time: %s" % configuration['alarm'])

        self._send_time = configuration['sendTime']
        self._sample = configuration['sample']
        self._alarm = configuration['alarm']
        for sensor in self.sensors:
            for time in configuration['sensorSample']:
               if sensor.get_id() == time['sensor']:
                   lc = LoopingCall(sensor.generate_value)
                   lc.start(time['t'])

        self.send_time_routine = LoopingCall(self._send_info)
        self.send_time_routine.start(self._send_time)

        self.sample_routine = LoopingCall(self._get_info)
        self.sample_routine.start(self._sample)

    def _send_info(self):
        try:
            if self._values:
                print("Sending info to server")
                self._dealer.send_communication(self._values)
                self._clear_register()
        except Exception as e:
            print(e)

    def _get_info(self):
        print("Getting info from sensors")
        for sensor in self.sensors:
            self._values[sensor.get_id()] = sensor.get_values()

    def _clear_register(self):
        self._values = {}
        for sensor in self.sensors:
            sensor.clear_values()


def main():
    board1 = BoardProtocol('127.0.0.1', '5555', 1, 1, 3)
    board1._register_on_server()
    reactor.run()

if __name__ == '__main__':
    main()
