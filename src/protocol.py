class DataDealerProtocol(object):

    def __init__(self):


    def _manage_connection(self, hot, port):
        """
        Create socket connetion with the server
        """
        pass

    def _register_board(self):
        """
        Register the board with the server
        """
        pass

    def _configure_board(self, config):
        """
        Configure the board and sensors with the info sent by server
        """

    def _store_value(self, sensor, value):
        """
        Store value from specific sensor
        """

    def _remote_update(self):
        """
        Send all data store on board to the server
        """
