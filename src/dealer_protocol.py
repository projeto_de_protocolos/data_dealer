from pyasn1.codec.ber import encoder, decoder
from pyasn1.codec.native import encoder as native_encoder
from asn1_b import *
from binascii import hexlify

import zmq

class DealerProtocol(object):

    def __init__(self, host, port, type):
        if type == 'server':
            self._configure_connection_rep(host, port)
        elif type == 'client':
            self._configure_connection_req(host, port)

    def _configure_connection_rep(self, host, port):
        try:
            address = "tcp://" + str(host) + ":" + str(port)
            context = zmq.Context.instance()
            self.socket = context.socket(zmq.REP)
            self.socket.bind(address)
        except Exception as e:
            print("Failed to bind to host [%s:%s]: %s" % (str(host),
                                                          str(port), e))

    def _configure_connection_req(self, host, port):
        try:
            address = "tcp://" + str(host) + ":" + str(port)
            context = zmq.Context.instance()
            self.socket = context.socket(zmq.REQ)
            self.socket.connect(address)
        except Exception as e:
            print("Failed to connect to host [%s:%s]: %s" % (str(host),
                                                             str(port), e))

    def _get_board_asn1(self, id, sensor_list):
        board = Identification().subtype(
            implicitTag=tag.Tag(tag.tagClassContext,
                                tag.tagFormatConstructed, 0))
        board.setComponentByName('boardId', id)
        board['sensors'].extend(sensor_list)
        return board

    def _get_sensors_asn1(self, sensors):
        new_sensors = []
        for sensor in sensors:
            std_sensor = SensorDesc()#.subtype(implicitTag=tag.Tag(
                #tag.tagClassContext, tag.tagFormatSimple, 1))
            std_sensor.setComponentByName('id', sensor[0])
            std_sensor.setComponentByName('tipo', sensor[1])
            new_sensors.append(std_sensor)
        return new_sensors

    def _get_time_asn1(self, data):
        new_time_list = []
        for item in data:
            new_time = Time()
            new_time.setComponentByName('sensor', item[0])
            new_time.setComponentByName('t', item[1])
            new_time_list.append(new_time)
        return new_time_list

    def _get_confirmation_asn1(self, send_time, sample, sensor_list, alarm):
        confirm = Confirmation().subtype(
            implicitTag=tag.Tag(tag.tagClassContext,
                                tag.tagFormatConstructed, 1))
        confirm.setComponentByName('sendTime', send_time)
        confirm.setComponentByName('sample', sample)
        confirm['sensorSample'].extend(sensor_list)
        confirm.setComponentByName('alarm', alarm)
        return confirm

    def _get_data_asn1(self, data):
        new_data = Data()
        new_data.setComponentByName('value', data[0])
        new_data.setComponentByName('timeStamp', data[1])
        return new_data

    def _get_leitor_asn1(self, id, values):
        leitor = Leitor()
        leitor.setComponentByName('sensor', id)
        leitor['leitura'].extend(values)
        return leitor

    def send_identification(self, data):
        prot = self._build_identification(data)
        self.transmit(prot)

    def send_confirmation(self, data):
        prot = self._build_confirmation(data)
        self.transmit(prot)

    def send_communication(self, data):
        prot = self._build_communication(data)
        self.socket.send(prot)
        self.socket.recv()

    def _build_identification(self, data):
        sensor_asn1 = self._get_sensors_asn1(data[1])
        board_asn1 = self._get_board_asn1(data[0], sensor_asn1)

        prot = Protocol()
        prot.setComponentByName('identification', board_asn1)
        prot = encoder.encode(prot)
        return prot

    def _build_confirmation(self, data):
        time_asn1 = self._get_time_asn1(data['time'])
        confirm_asn1 = self._get_confirmation_asn1(data['sendTime'],
                                                   data['sample'],
                                                   time_asn1,
                                                   data['alarm'])

        prot = Protocol()
        prot.setComponentByName('confirmation', confirm_asn1)
        prot = encoder.encode(prot)
        return prot

    def _build_communication(self, data):
        reads = []
        for key, values in data.items():
            datas = []
            for value in values:
                new_data = self._get_data_asn1(value)
                datas.append(new_data)
            reads.append(self._get_leitor_asn1(key, datas))
        communication = Communication().subtype(
            implicitTag=tag.Tag(tag.tagClassContext,
                                tag.tagFormatConstructed, 2))
        communication['values'].extend(reads)

        prot = Protocol()
        prot.setComponentByName('communication', communication)
        prot = encoder.encode(prot)
        return prot

    def recv(self):
        message = self.socket.recv()
        return self._proccess_data(message)

    def _proccess_data(self, data):
        message = decoder.decode(data, asn1Spec=Protocol())[0]
        if message.getName() == 'identification':
            print("Identification received")
            new_data = self._handle_identification(message)
        elif message.getName() == 'confirmation':
            print("Confirmation received")
            new_data = self._handle_confirmation(message)
        elif message.getName() == 'communication':
            print("Communication received")
            new_data = self._handle_communication(message)
        if new_data:
            return new_data

    def transmit(self, data):
        self.socket.send(data)

    def send_ok(self):
        self.socket.send_string('ok')

    def _handle_identification(self, identification):
        message = identification.getComponentByName('identification')
        board_id = native_encoder.encode(message['boardId'])
        asn1_sensor_list = message['sensors']
        board_sensor_list = []
        for item in asn1_sensor_list:
            native_tuple = native_encoder.encode(item)
            board_sensor_list.append(native_tuple)

        converted_data = {'identification': (board_id, board_sensor_list)}
        return converted_data

    def _handle_confirmation(self, confirmation):
        message = confirmation.getComponentByName('confirmation')
        message = native_encoder.encode(message)
        return message

    def _handle_communication(self, communication):
        message = communication.getComponentByName('communication')
        message = native_encoder.encode(message)
        message = {'communication': message}
        return message
