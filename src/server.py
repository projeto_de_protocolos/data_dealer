import json
import zmq
import threading
import sys
import random
from asn1_b import *
from pyasn1.codec.ber import encoder, decoder
from dealer_protocol import DealerProtocol


class Sensor(object):
    def __init__(self, id, type, board):
        self.id = id
        self.type = type
        self.board = board
        self.values = []

    def set_value(self, value):
        self.values.append(value)

class BasicStorageServer(object):

    def __init__(self):
        host = '*'
        port = '5555'
        self._dealer = DealerProtocol(host, port, 'server')
        self.sensor_dict = {}
        thread_server = threading.Thread(target=self._main_loop())
        thread_server.start()

    def _main_loop(self):
        while True:
            #  Wait for next request from client
            print('Waiting items to store...')
            message = self._dealer.recv()
            for key, value in message.items():
                self._handle_message(key, value)

    def _handle_message(self, message_type, value):
        if message_type == 'identification':
            board_id = value[0]
            time = []
            print("INFO: Received registration from Board #%s." % board_id)
            for item in value[1]:
                new_sensor = Sensor(item['id'], item[ 'tipo'], board_id)
                self.sensor_dict[item['id']] = new_sensor
                t = random.randint(3,7)
                time.append((item['id'], t))
            response = {}
            response['sendTime'] = random.randint(10,16)
            response['sample'] = random.randint(5,11)
            response['alarm'] = random.randint(10, 20)
            response['time'] = time
            
            print("INFO: Sending configuration to Board #%s" % board_id)
            print("Send time: %s" % response['sendTime'])
            print("Sample time: %s" % response['sample'])
            print("Alarm time: %s" % response['alarm'])

            self._dealer.send_confirmation(response)

        elif message_type == 'communication':
            for key, values in value.items():
                for info in values:
                    print("Received from sensor #%s: %s" % (info['sensor'],
                                                            info['leitura']))
            self._dealer.send_ok()
        else:
            pass

if __name__ == '__main__':
    BasicStorageServer()

